public class Calculator 
{
	public static int addTwo(int firstVal, int secondVal)
	{
		int sum = firstVal + secondVal;
		return sum;
	}
	
	public int subtractTwo(int firstVal, int secondVal)
	{
		int difference = firstVal - secondVal;
		return difference;
	}
	
	public static int multiplyTwo(int firstVal, int secondVal)
	{
		int product = firstVal * secondVal;
		return product;
	}
	
	public double divideTwo(double firstVal, double secondVal)
	{
		double quotient = firstVal / secondVal;
		return quotient;
	}
}
