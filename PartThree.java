import java.util.Scanner;

public class PartThree
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		Calculator calc = new Calculator();
		int firstVal = scan.nextInt();
		int secondVal = scan.nextInt();
		System.out.println(Calculator.addTwo(firstVal,secondVal));
		System.out.println(calc.subtractTwo(firstVal,secondVal));
		System.out.println(Calculator.multiplyTwo(firstVal,secondVal));
		System.out.println(calc.divideTwo(firstVal,secondVal));
	}
}